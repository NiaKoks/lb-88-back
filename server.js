const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const forumposts = require('./app/posts');
const users = require('./app/users');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const port = 8000;

mongoose.connect('mongodb://localhost/Forum',{useNewUrlParser:true}).then(()=>{
   app.use('/posts',forumposts);
   app.use('/users',users);

   app.listen(port,()=>console.log(`Server runned on ${port} port`))
});