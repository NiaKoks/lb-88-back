const mongoose = require('mongoose');
const config = require('./config');

const Post = require('./models/ForumPost');
const User = require('./models/User');

const run =async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for(let collection of collections){
        await collection.drop();
    }
};

run().catch(error =>{
    console.log('Something wrong happened ...' ,error);
});