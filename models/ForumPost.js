const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ForumPostSchema = new Schema ({
   title:{
    type: String,
    required: true
   },
   description:{
       type: Text,
       required: true,
       validator:{

       }
   },
   image:{
       type:Text,
       required:true,
       validator:{

       }
   }

});

const ForumPost = mongoose.model('ForumPost',ForumPostSchema);
module.exports = ForumPost;