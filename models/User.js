const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const nanoid = require('nanoid');

const Schema = mongoose.Schema;
const SALT_ROUNDS = 10;

const UserSchema = new Schema ({
   username:{
      type: String,
      required: true,
      unique: true
   },
    password:{
      type: String,
      required: true
    },
    token:{
       type: String,
       required: true
    }

});